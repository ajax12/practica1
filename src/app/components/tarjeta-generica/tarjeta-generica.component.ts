import { Component, EventEmitter, Input, OnInit, Output,} from '@angular/core';
import { Router } from '@angular/router';
import { Heroe } from 'src/app/servicios/heroe.service';

@Component({
  selector: 'app-tarjeta-generica',
  templateUrl: './tarjeta-generica.component.html'
 
})
export class TarjetaGenericaComponent implements OnInit {

  @Input() item:Heroe;
  @Input() index: number;

  @Output() heroeSeleccionado: EventEmitter<number>; 

  constructor(private router: Router) {
    this.heroeSeleccionado = new EventEmitter();    
  }

  ngOnInit(): void {
  }


  verHeroe(){
    this.router.navigate(['/heroe',this.index]);
    //this.heroeSeleccionado.emit(this.index);

  }


}

