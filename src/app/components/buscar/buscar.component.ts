import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Heroe, HeroeService } from 'src/app/servicios/heroe.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html'
  
})
export class BuscarComponent implements OnInit {
  item: Heroe[] = [];
  termino: string;

  constructor(private heroeService: HeroeService, private activatedRouted: ActivatedRoute){

  }

  ngOnInit(): void {
    
    this.activatedRouted.params.subscribe(params =>{
      this.termino = params['termino'];
      this.item = this.heroeService.buscar(params['termino']);
    })

  }

}
