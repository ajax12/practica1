import { Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroeService } from 'src/app/servicios/heroe.service';


@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
  
})
export class HeroeComponent {

  heroe: any={};

  constructor( private activatedRouted: ActivatedRoute, private heroeService: HeroeService) {

    this.activatedRouted.params.subscribe( params =>{
      console.log(params['id']);
      this.heroe = this.heroeService.getHeroe(params['id']);

    });

   }



  

}
