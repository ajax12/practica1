import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Heroe, HeroeService } from 'src/app/servicios/heroe.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
  
})
export class HeroesComponent implements OnInit {

  heroes: Heroe[] = [];
  

  constructor( private heroeService: HeroeService, private router: Router) { 
  }

  ngOnInit(){

    this.heroes = this.heroeService.getheroes();
    console.log(this.heroes);
  }

  verHeroe(index: number){
     this.router.navigate(['/heroe',index]);

  }

  
  



  

}
